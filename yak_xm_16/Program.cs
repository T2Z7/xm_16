﻿using System;
//Составить программу для вычисления суммы всех натуральных чисел, кратных числу(%) b и
//меньших 100.Выполнить анализ решения и вычислить метрики решения.Разработанное
//приложение разместить в репозитории
namespace yak_xm_16
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите натуральное (больше 0) число, меньшее 100");
start:      int b = Convert.ToInt32(Console.ReadLine());
            if (b < 1 || b > 99) goto start;
            int sum = 0;
            for (int i = 1; i < 100; i++ )
            {
                if (i % b == 0) sum = sum + i; 
            }
            Console.WriteLine("Сумма натуральных чисел, кратных числу {0} равна {1}", b, sum );
        }
    }
}
